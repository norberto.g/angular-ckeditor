import { Component, OnInit } from '@angular/core';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { UploadAdapter } from './lib/UploadAdapter'

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent{
  article = {text:'', title:''}
  constructor() {
    this.article = {
      text: 'test',
      title: '',
    };
   }
  public Editor = DecoupledEditor;
  
  editorConfig = {
    simpleUpload: {
      uploadUrl: 'http://example.com',
      headers: {
          'Content-Type':'application/json'
      }
    }
    //,removePlugins: [ 'ImageUpload','Image', 'ImageToolbar', 'ImageStyle' ],
  };
  

  public onReady( editor ) {
    // Load the editor on dom element
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement()
    );
    // Declare and attach custom upload adapter to editor instance
    editor.plugins.get( 'FileRepository' ).createUploadAdapter = loader => {
      return new UploadAdapter( loader, editor.config.get( 'simpleUpload').uploadUrl, editor.t );
    }
  }

}
