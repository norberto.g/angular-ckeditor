import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-content-box',
  templateUrl: './content-box.component.html',
  styleUrls: ['./content-box.component.css']
})
export class ContentBoxComponent implements OnInit {
  @Input() item = {
    'name':'',
    'id':null,
    'position': null,
    'content': null
  };
  constructor() { }

  ngOnInit(): void {
  }

}
