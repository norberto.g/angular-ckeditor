import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  data = []
  title = "Dashboard"
  bundle = {
    "title":"some shit",
    "items":[
      {
        "id": 1,
        "name": "item 1",
        "position": 1,
        "content":"<h2>A subtitle</h2><p>This is a text</p>"
      },
      {
        "id": 2,
        "name": "item 5",
        "position": 5,
        "content":"<hr/>"
      },
      {
        "id": 3,
        "name": "item 2",
        "position": 2,
        "content":"<i>Italic font</i>"
      },
      {
        "id": 4,
        "name": "item 4",
        "position": 4,
        "content":"<ul><li>Item loco</li><li>Item seee</li></ul>"
      },
      {
        "id": 5,
        "name": "item 3",
        "position": 3,
        "content": "<b>strong</b>"
      }
    ]
  }
  constructor() { 

    this.data = this.bundle.items.sort((a, b)=> a.position - b.position);

  }
}
